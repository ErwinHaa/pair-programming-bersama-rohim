import { BAGI, TAMBAH, KALI, KURANG } from '../reducers/calculator';

export const handleTambah = (values) => {
  return {
    type: TAMBAH,
    payload: values,
  };
};

export const handleKurang = (values) => {
  return {
    type: KURANG,
    payload: values,
  };
};

export const handleKali = (values) => {
  return {
    type: KALI,
    payload: values,
  };
};

export const handleBagi = (values) => {
  return {
    type: BAGI,
    payload: values,
  };
};
