import { INCREMENT, DECREMENT, RESET } from '../reducers/counter'

export const handleIncrement = () => {
    return {
        type: INCREMENT
    }
}

export const handleDecrement = () => {
    return {
        type: DECREMENT
    }
}

export const handleReset = () => {
    return {
        type: RESET
    }
}