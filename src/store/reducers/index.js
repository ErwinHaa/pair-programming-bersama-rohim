import { combineReducers } from 'redux';
import counterReducer from './counter';
import calculatorReducer from './calculator';

export default combineReducers({
  counter: counterReducer,
  calculator: calculatorReducer,
});
