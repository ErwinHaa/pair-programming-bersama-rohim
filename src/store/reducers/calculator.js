export const TAMBAH = 'TAMBAH';
export const KURANG = 'KURANG';
export const KALI = 'KALI';
export const BAGI = 'BAGI';

const calculatorReducer = (state = 0, action) => {
  const { type, payload } = action;
  let result;
  switch (type) {
    case TAMBAH:
      result = parseInt(payload.x) + parseInt(payload.y);
      return result;
    case KURANG:
      result = parseInt(payload.x) - parseInt(payload.y);
      return result;
    case KALI:
      result = parseInt(payload.x) * parseInt(payload.y);
      return result;
    case BAGI:
      result = parseInt(payload.x) / parseInt(payload.y);
      return result;
    default:
      return state;
  }
};

export const selectResult = (state) => state.calculator;

export default calculatorReducer;
