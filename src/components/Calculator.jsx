import { useState } from 'react';
import { Route, useRouteMatch } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Container } from 'reactstrap';

import CalculatorControl from './CalculatorControl';
import { selectResult } from '../store/reducers/calculator';
import {
  handleTambah,
  handleKurang,
  handleKali,
  handleBagi,
} from '../store/actions/calculator';
import Counter from './Counter';

const Calculator = () => {
  const match = useRouteMatch();
  const [inputX, setInputX] = useState(0);
  const [inputY, setInputY] = useState(0);
  const [operator, setOperator] = useState('');

  const result = useSelector(selectResult);
  const dispatch = useDispatch();

  const handleXChange = (e) => {
    setInputX(e.target.value);
  };

  const handleYChange = (e) => {
    setInputY(e.target.value);
  };

  const handleOperatorChange = (e) => {
    setOperator(e.target.value);
  };

  const handleButtonClick = (e) => {
    // handleCalculate() {
    //
    //}

    switch (operator) {
      case '+':
        dispatch(handleTambah({ x: inputX, y: inputY }));
        break;
      case '-':
        dispatch(handleKurang({ x: inputX, y: inputY }));
        break;
      case 'X':
        dispatch(handleKali({ x: inputX, y: inputY }));
        break;
      case '/':
        dispatch(handleBagi({ x: inputX, y: inputY }));
        break;
      default:
        return;
    }
  };

  return (
    <Container>
      <h1>Result: {result}</h1>
      <CalculatorControl
        xValue={inputX}
        yValue={inputY}
        onXChange={handleXChange}
        onYChange={handleYChange}
        onOperatorChange={handleOperatorChange}
        onClick={handleButtonClick}
        operator={operator}
      />
      <Route path={`${match.path}/:slug`}>
        <Counter />
      </Route>
    </Container>
  );
};

export default Calculator;
