import { Button } from 'reactstrap';
import { Route, useRouteMatch, useHistory } from 'react-router-dom';
import Calculator from './Calculator';
import Counter from './Counter';

const Home = () => {
  const match = useRouteMatch();
  const history = useHistory();

  const handleClick = (slug) => {
    history.push(`${match.url}${slug}`);
  };

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
      }}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          width: '50%',
          padding: '50px 0',
        }}>
        <Button onClick={() => handleClick('/calculator')}>
          Show Calculator
        </Button>
        <Button onClick={() => handleClick('/counter')}>Show Counter</Button>
      </div>
      <Route exact path={`${match.path}/calculator`}>
        <Calculator />
      </Route>
      <Route exact path={`${match.path}/counter`}>
        <Counter />
      </Route>
    </div>
  );
};

export default Home;
