import { Navbar, Nav, NavItem, NavbarBrand } from 'reactstrap';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <Navbar color="light" light expand="md">
      <NavbarBrand to="/">reactstrap</NavbarBrand>
      <Nav className="mr-auto" navbar>
        <NavItem>
          <Link to="/hitung/calculator">Calculator</Link>
        </NavItem>
        <NavItem>
          <Link to="/hitung/counter">Counter</Link>
        </NavItem>
      </Nav>
    </Navbar>
  );
};

export default Header;
