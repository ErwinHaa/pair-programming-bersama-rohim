import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import userEvent from '@testing-library/user-event';
import App from '../components/App';
import store from '../store';

beforeEach(() =>
  render(
    <Router>
      <Provider store={store}>
        <App />
      </Provider>
    </Router>
  )
);

describe('<App />', () => {
  it('should render without problems', () => {});

  it('seharusnya kita klik tambah maka angka akan bertambah', () => {
    const tombolTambah = screen.getByRole('button', { name: '+' });
    const hasil = screen.getByText('0');
    userEvent.click(tombolTambah);
    expect(hasil).toHaveTextContent('1');
  });

  it('seharusnya klo klik kurang angkanya berkurang', () => {
    const tombolKurang = screen.getByRole('button', { name: '-' });
    const tombolTambah = screen.getByRole('button', { name: '+' });
    const hasil = screen.getByText('1');
    userEvent.click(tombolKurang);
    userEvent.click(tombolTambah);
    userEvent.click(tombolTambah);
    expect(hasil).toHaveTextContent('2');
  });

  it('should show zero result', () => {
    const displayResult = screen.getByRole('heading', { name: /result/i });

    expect(displayResult).toHaveTextContent('0');
  });

  it('should display input value as typed', () => {
    const inputX = screen.getByLabelText(/input x/i);
    const inputY = screen.getByLabelText(/input y/i);

    userEvent.type(inputX, '30');

    expect(inputX).toHaveValue(30);

    userEvent.type(inputY, '50');

    expect(inputY).toHaveValue(50);
  });

  it('should display correct operator when selected', () => {
    const operator = screen.getByRole('combobox');

    userEvent.selectOptions(operator, '+');

    expect(operator).toHaveValue('+');

    userEvent.selectOptions(operator, '-');

    expect(operator).toHaveValue('-');
  });

  it('should calculate correct result when button is clicked', () => {
    const inputX = screen.getByLabelText(/input x/i);
    const inputY = screen.getByLabelText(/input y/i);
    const operator = screen.getByRole('combobox');
    const button = screen.getByRole('button');
    const displayResult = screen.getByRole('heading', { name: /result/i });

    userEvent.type(inputX, '30');
    userEvent.type(inputY, '50');
    userEvent.selectOptions(operator, '+');
    userEvent.click(button);

    expect(displayResult).toHaveTextContent(/result: 80/i);
  });
});
