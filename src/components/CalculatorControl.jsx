import { Button, Label, Input, FormGroup } from 'reactstrap';

const CalculatorControl = ({
  xValue,
  yValue,
  operator,
  onXChange,
  onYChange,
  onOperatorChange,
  onClick,
}) => {
  return (
    <div className="d-flex flex-column justify-content-content align-items-center">
      <FormGroup className="w-50">
        <Label htmlFor="xInput" className="align-self-start">
          Input X
        </Label>
        <Input type="number" value={xValue} onChange={onXChange} id="xInput" />
      </FormGroup>
      <FormGroup className="w-50">
        <Label htmlFor="yInput" className="align-self-start">
          Input Y
        </Label>
        <Input type="number" value={yValue} onChange={onYChange} id="yInput" />
      </FormGroup>
      <FormGroup className="w-50">
        <Label htmlFor="operator" className="align-self-start">
          Select Operator
        </Label>
        <Input
          type="select"
          value={operator}
          onChange={onOperatorChange}
          id="operator">
          <option>---</option>
          <option>+</option>
          <option>-</option>
          <option>X</option>
          <option>/</option>
        </Input>
      </FormGroup>
      <Button onClick={onClick}>Calculate</Button>
    </div>
  );
};

export default CalculatorControl;
