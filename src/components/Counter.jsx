import { Button } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import {
  handleIncrement,
  handleDecrement,
  handleReset,
} from '../store/actions/counter';

const Counter = () => {
  const counter = useSelector((state) => state.counter);
  const dispatch = useDispatch();

  const handlePlus = (e) => {
    dispatch(handleIncrement());
  };

  const handleUlang = (e) => {
    dispatch(handleReset());
  };

  const handleMinus = (e) => {
    dispatch(handleDecrement());
  };

  return (
    <div>
      <h2> Counter </h2>
      <h3>{counter}</h3>
      <Button onClick={handlePlus}> + </Button>
      <Button onClick={handleUlang}> Reset </Button>
      <Button onClick={handleMinus}> - </Button>
    </div>
  );
};

export default Counter;
