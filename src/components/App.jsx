import { Route, Switch, Redirect } from 'react-router-dom';

import Home from './Home';

const App = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Redirect exact to="/hitung" />
        </Route>
        <Route path="/hitung">
          <Home />
        </Route>
      </Switch>
    </div>
  );
};

export default App;
